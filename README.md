
<pre>
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ This project has been superceded by NDD Maven (https://gitlab.com/ndd-oss/java/ndd-maven) ┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
</pre>

# NDD Maven Java

Common Maven configuration for NDD Java projects.

The documentation of the master branch is [available on GitLab Pages][ndd-gitlab-pages].

The documentations of the other branches are [available in the GitLab Environments section][ndd-gitlab-environments].
The environments of these branches are accessible in one of the `Available` or `Stopped` tabs and are displayed by clicking on the `Open live environment` button at the right.



[ndd-gitlab-environments]: https://gitlab.com/ddidier/java-ndd-maven-java/-/environments
[ndd-gitlab-pages]: https://ddidier.gitlab.io/java-ndd-maven-java/
