
NDD Maven Java
==============

## Version 0.4.0

- ci: add CI build
- refactor: refactor all and rename to ndd-maven-java
- build: move from BitBucket to GitLab

## Version 0.3.1

- [DEPENDENCY] Add DbUnit and H2 dependencies

## Version 0.3.0

- [DEPENDENCY] Update plugins versions
- [DEPENDENCY] Update POM to use Java 8

## Version 0.2.4

- [DEPENDENCY] Updated parent and dependencies versions

## Version 0.2.3

- [QUALITY] Added PMD, see [NDD-PARENT-JAVA-2](https://bitbucket.org/ndd-java/ndd-parent-java/issue/2/add-pmd-report)
- [PROJECT] Renamed BitBucket project from `ndd-java-parent` to `ndd-parent-java`

## Version 0.2.2

- [DOCUMENTATION] Added this changelog
- [DEPENDENCY] Added Apache Commons IO
- [DEPENDENCY] Added PowerMock

## Version 0.2.1

- [DEPENDENCY] Updated the version of the `ndd-build` dependency to `0.2.1`

## Version 0.2.0

- initial commit
